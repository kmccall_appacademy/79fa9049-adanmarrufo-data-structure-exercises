# EASY

# Write a method that returns the range of its argument (an array of integers).
def range(arr)
  return nil if arr.empty?
  lowest = arr[0]
  highest = arr[0]
  arr.each do |element|
    lowest = element if element < lowest
    highest = element if element > highest
  end
  # your code goes here
  highest - lowest
end

# Write a method that returns a boolean indicating whether an array is in sorted
# order. Use the equality operator (==), which returns a boolean indicating
# whether its operands are equal, e.g., 2 == 2 => true, ["cat", "dog"] ==
# ["dog", "cat"] => false
def in_order?(arr)
  return true if arr.empty?
  previous_elem = arr[0]
  arr.each do |element|
    return false if element < previous_elem
    previous_elem = element
  end
  true
end


# MEDIUM

# Write a method that returns the number of vowels in its argument
def num_vowels(str)
  vowels = %w(a e i o u)
  vowel_count = 0
  str.each_char do |letter|
    vowel_count += 1 if vowels.include? letter.downcase
  end
  vowel_count
end

# Write a method that returns its argument with all its vowels removed.
def devowel(str)
  vowels = %w(a e i o u)
  str.chars.reject { |letter| vowels.include? letter.downcase }.join
end


# HARD

# Write a method that returns the returns an array of the digits of a
# non-negative integer in descending order and as strings, e.g.,
# descending_digits(4291) #=> ["9", "4", "2", "1"]
def descending_digits(int)
  # your code goes here
  digits = []
  loop do
    digits << (int % 10).to_s
    int /= 10
    break if int == 0
  end
  digits.sort.reverse!
end

# Write a method that returns a boolean indicating whether a string has
# repeating letters. Capital letters count as repeats of lowercase ones, e.g.,
# repeating_letters?("Aa") => true
def repeating_letters?(str)
  previous_letter = str[0].downcase if not str.empty?
  1.upto(str.length - 1) do |i|
    return true if str[i].downcase == previous_letter
    previous_letter = str[i].downcase
  end
  false
end

# Write a method that converts an array of ten integers into a phone number in
# the format "(123) 456-7890".
def to_phone_number(arr)
  "(#{arr[0, 3].join}) #{arr[3, 3].join}-#{arr[6, 4].join}"
end

# Write a method that returns the range of a string of comma-separated integers,
# e.g., str_range("4,1,8") #=> 7
def str_range(str)
  range(str.split(",").map(&:to_i))
end


#EXPERT

# Write a method that is functionally equivalent to the rotate(offset) method of
# arrays. offset=1 ensures that the value of offset is 1 if no argument is
# provided. HINT: use the take(num) and drop(num) methods. You won't need much
# code, but the solution is tricky!
def my_rotate(arr, offset = 1)
  if offset >= 0
    arr.drop(offset % arr.length) + arr.take(offset % arr.length)
  else
    arr.drop(arr.length - offset.abs % arr.length) +
    arr.take(arr.length - offset.abs % arr.length)
  end
end
